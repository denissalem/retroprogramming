Z_LIB_SOURCE_DIR = 

CC       = wcc
LINKER   = wcl
CFLAGS   = -zq -ml -bt=dos -oilrtfm -wx -3
LDFLAGS  = -bcl=dos

C_SOURCES = adler32.c	 &
            compress.c &
            crc32.c		 &
            deflate.c	 &
            gzclose.c	 &
            gzlib.c		 &
            gzread.c	 &
            gzwrite.c	 &
            infback.c	 &
            inffast.c	 &
            inflate.c	 &
            inftrees.c &
            trees.c	   &
            uncompr.c	 &
            zutil.c

OBJS =  adler32.o  &
        compress.o &
        crc32.o    &
        deflate.o   &
        gzclose.o  &
        gzlib.o    &
        gzread.o   &
        gzwrite.o  &
        infback.o  &
        inffast.o   &
        inflate.o   &
        inftrees.o &
        trees.o    &
        uncompr.o  &
        zutil.o

ZLIB_LIB = zlib.lib
ZLIB_TEST = zlib_test.exe

.c : $(Z_LIB_SOURCE_DIR)

.c.o :
        $(CC) $(CFLAGS) $[@

all : $(ZLIB_LIB) $(ZLIB_TEST)

$(ZLIB_LIB) : $(OBJS)
        wlib -q -b -c -n $(ZLIB_LIB) -+adler32.o  -+compress.o -+crc32.o
        wlib -q -b -c $(ZLIB_LIB) -+gzclose.o  -+gzlib.o    -+gzread.o   -+gzwrite.o
        wlib -q -b -c $(ZLIB_LIB) -+deflate.o  -+infback.o
        wlib -q -b -c $(ZLIB_LIB) -+inffast.o  -+inflate.o  -+inftrees.o
        wlib -q -b -c $(ZLIB_LIB) -+trees.o    -+uncompr.o  -+zutil.o
        rm -f $(OBJS)

clean : .SYMBOLIC
        rm -f $(OBJS) test_zlib.o
        rm -f *err
        rm -f $(ZLIB_LIB)
        rm -f $(ZLIB_TEST)
        
$(ZLIB_TEST) : $(ZLIB_LIB)
		 $(CC) $CFLAGS ../code/test_zlib.c 
		 $LINKER $(LDFLAGS) -fe=$(ZLIB_TEST) test_zlib.o $(ZLIB_LIB)
		 
