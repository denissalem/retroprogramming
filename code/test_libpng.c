#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <conio.h>
#include <dos.h>
#include <i86.h> 

#include <zlib.h>
#include <png.h>

/* wcl -zq -ml -bt=dos -oilrtfm -wx -3 test_libpng.c $WATCOM/lib286/dos/zlib.lib $WATCOM/lib286/dos/libpng.lib -fe=png.exe */

/* code based on this: http://www.libpng.org/pub/png/book/chapter13.html */

#define CMP_COLOR(color1, color2) (color1.red != color2.red || color1.green != color2.green || color1.blue != color2.blue)

typedef unsigned char byte;

typedef struct Asset_t {
    png_uint_32 width;  
    png_uint_32 height;
    int bit_depth;
    int color_type;
    int status;
    byte * data;
} Asset;

static png_color colors[256] = {0};
static unsigned char color_count = 0;

void text_mode() {
    union REGS regs;
    regs.h.ah = 0x00;
    regs.h.al = 0x03; /* text mode is mode 3 */
    int86(0x10,&regs,&regs);
}

void mode13() {
    union REGS regs;
    int i;
    regs.h.ah = 0x00;  /* function 00h = mode set */
    regs.h.al = 0x13;  /* 256-color */
    int86(0x10,&regs,&regs);
    
    // Update VGA palette
    outp(0x03c8, 0);
    for(i=0; i<256; i++)
    { 
      outp(0x03c9, colors[i].red >> 2);
      outp(0x03c9, colors[i].green >> 2);
      outp(0x03c9, colors[i].blue >> 2 );
    }
}

void draw_loop(Asset * image) {
    byte far *VGA = (byte far*) 0xA0000000L;
    byte far *VGA_double_buffer = (byte far*) malloc(64000 * sizeof(byte));
    long unsigned int x,y;
        
    memset(VGA_double_buffer, 0, 64000);
    memset(VGA, 0, 64000);

    while (!kbhit()) {
        for (y=0; y<image->height; y++) {
            for (x=0; x<image->width; x++) {
                if (image->bit_depth == 4) {
                    if (x%2) {
                        VGA_double_buffer[x+y*320] = (image->data[(x>>1)+ ((y*image->width) >> 1)]) & 0xF;
                    }
                    else {
                        VGA_double_buffer[x+y*320] = (image->data[(x>>1)+ ((y*image->width)>>1)] ) >> 4;
                    }
                }
            }
        }
        memcpy(VGA,VGA_double_buffer,64000);
    }
    free(VGA_double_buffer);
}

void readpng(FILE * f, Asset * image) {
    unsigned char sig[8];
    png_structp png_ptr;
    png_infop info_ptr;
    png_uint_32 rowbytes;
    png_bytep * rows;
    png_colorp palette;
    int num_palette;
    int i,j;
    int match = 0;
    
    fread(sig, 1, 8, f);
    if (!png_check_sig(sig, 8)) {
        printf("png_check_sig failed.\n");
        image->status = 1;
        return;
    }
    
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        printf("png_create_read_struct failed.\n");
        image->status = 4;
        return;
    }
    
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        printf("png_create_info_struct failed.\n");
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        image->status = 4;
        return;   /* out of memory */
    }

    png_init_io(png_ptr, f);
    
    png_set_sig_bytes(png_ptr, 8);
        
    png_read_info(png_ptr, info_ptr);
        
    png_get_IHDR(
        png_ptr,
        info_ptr,
        &image->width,
        &image->height,
        &image->bit_depth,
        &image->color_type,
        NULL,
        NULL,
        NULL
    );

    rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    rows = (png_bytep *) malloc(sizeof(png_bytep) * image->height);
    image->data = (unsigned char *) malloc(rowbytes * image->height);
    
    if ( image->data == NULL) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return;
    }
    
    for (i = 0;  i < image->height;  ++i) {
        rows[i] = image->data + i*rowbytes;
    }
    
    png_read_image(png_ptr, rows);
    if (image->color_type == PNG_COLOR_TYPE_PALETTE) {
        png_get_PLTE(
            png_ptr,
            info_ptr,
            &palette,
            &num_palette
        );
        
        for(j= 0; j < num_palette; j++) {
            for (i = 0; i < color_count; i++) {
                if (!CMP_COLOR(colors[i], palette[j])) {
                    match = 1;
                    break;
                }
            }
            
            if (!match) {
                colors[color_count++] = palette[j];
            }
        }
    }
    
    free(rows);
}

int main () {
  FILE * f = NULL;
  Asset troll = {0};
  int i;
  printf("zlibVersion(): %s\n", zlibVersion());
  printf("PNG_LIBPNG_VER_STRING: %s\n", PNG_LIBPNG_VER_STRING);
  printf("png_libpng_ver: %s\n", png_libpng_ver);
  
  f = fopen("troll.png", "rb");
  
  if (f == NULL) {
      printf("Damn bruh, can't open file!\n");
      return -1;
  }

  readpng(f, &troll);
  if(troll.status) {
      printf("Damn bruh !\n");
      return -1;
  }
  
  fclose(f);
  
  mode13();
  draw_loop(&troll);
  text_mode();
  for(i=0; i<color_count; i++) 
      printf("%X %X %X\n", colors[i].red & 0xFF, colors[i].green & 0xFF, colors[i].blue & 0xFF);

  return 0;
}
