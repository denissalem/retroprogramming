LIBPNG_SOURCE_DIR = 
LIBPNG_TARGET_LIB_DIR = 
LIBPNG_TARGET_INCLUDE_DIR = 

CC         = wcc
LINKER     = wcl
CFLAGS     =  -zq -ml -bt=dos -oilrtfm -wx -3 #-DPNG_DEBUG=2

LIBPNG_LIB = libpng.lib

OBJS_1 = png.o      &
         pngset.o   &
         pngget.o   &
         pngrutil.o &
         pngtrans.o &
         pngwutil.o
         
OBJS_2 = pngmem.o   &
         pngpread.o &
         pngread.o &
         pngerror.o &
         pngwrite.o
         
OBJS_3 = pngrtran.o &
         pngwtran.o &
         pngrio.o   &
         pngwio.o

H =      $(LIBPNG_SOURCE_DIR)png.h &
         $(LIBPNG_SOURCE_DIR)pnglibconf.h &
         $(LIBPNG_SOURCE_DIR)pngconf.h &
         $(LIBPNG_SOURCE_DIR)pngstruct.h &

CWD = $+ $(%CWD) $-

.BEFORE:
					cp ../code/pnglibconf-16-bits.h $(LIBPNG_SOURCE_DIR)pnglibconf.h

all : $(LIBPNG_LIB)

.c : $(LIBPNG_SOURCE_DIR)

.c.o:
         $(CC) $(CFLAGS) $[@

$(LIBPNG_LIB) : $(OBJS_1) $(OBJS_2) $(OBJS_3)
         wlib -b -c -n -q libpng.lib $(OBJS_1)
         wlib -b -c -q libpng.lib $(OBJS_2)
         wlib -b -c -q libpng.lib $(OBJS_3)


clean : .SYMBOLIC
         rm -f $(OBJS_1)
         rm -f $(OBJS_2)
         rm -f $(OBJS_3)
         rm -f *err
         rm -f $(LIBPNG_LIB)
         rm -f png.h pnglibconf.h pngconf.h

install: .SYMBOLIC
         rm -f $(OBJS_1)
         rm -f $(OBJS_2)
         rm -f $(OBJS_3)
         rm -f *err
         cp $(H) $(LIBPNG_TARGET_INCLUDE_DIR)
         mv $(LIBPNG_LIB) $(LIBPNG_TARGET_LIB_DIR)
