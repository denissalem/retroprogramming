#include <stdio.h>

int main(int argc, char **argv) {
    FILE * f = NULL;
    unsigned char buf[16] = {0};
    int check = 0;
    int i = 0;
    int line = 0;
    int chunk_size = 16;
    
    /* Do not forget the binary mode ! */
    f = fopen("troll.png", "rb");
    
    while(1) {
        printf("%d\t", ++line);
        check = fread(buf, 1, chunk_size, f);
        for (i = 0; i < chunk_size; i++) {
            printf("%X ", buf[i] & 0xFF);
        }
        printf("\n");
        if (check != chunk_size) {
            printf("Break with check = %d\n", check);
            break;
        }
    }
    return 0;
}
